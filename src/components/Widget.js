const Widget = (props) => {
    const { data: { title, text } } = props.dataToShowOnClick;
    return (
        <div className="widget">
            <div className="widget-container">
                <h3 className="widget-title">
                    {title}
                </h3>
                <p className="widget-text">
                    {text}
                </p>

            </div>
        </div>
    )
}

export default Widget
