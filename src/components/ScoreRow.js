import ProgressBarWithSection from "./ProgressWithSection";
import SectionDividerRow from "./SectionDividerRow";

const ScoreRow = (props) => {
    const imageComponent = (<img src={props.imageUrlDark} alt="success check mark" className="image-component" />);


    return (
        <>
            <div className="parent-widget-container loaded">
                <div className="widget-with-action">
                    <div className="left-section-row">
                        <i className="arrow left"></i>
                        <ProgressBarWithSection className="progress-bar" percentageScore={props.percentageScore} scoreColor={props.scoreColor} />

                    </div>
                    <div className="image-holder">
                        <h3 className="title" dangerouslySetInnerHTML={{ __html: props.title }} />

                        {imageComponent}
                    </div>
                </div>
            </div>
            {props.hasDivider && <SectionDividerRow />}
        </>
    )
}

export default ScoreRow
