
const UnexpandableRow = (props) => {

    return (
        <div className="widget-unexpandable-row">
                <h3 className="widget-title">
                    {props.title}
                </h3>
                <p className="widget-text">
                    {props.value}
                </p>
        </div>
    )
}

export default UnexpandableRow
