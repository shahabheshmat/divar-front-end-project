export const TitleRow = (props) => {

    return (
        <div className="widget-title-row">
            <div className="widget-container">
                <h3 className="widget-title">
                    {props.text}
                </h3>
            </div>
        </div>
    )
}

export default TitleRow

