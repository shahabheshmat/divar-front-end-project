
const ProgressWithSection = (props) => {
    const practiceScore = props.percentageScore;
    const blocksToColor = Math.floor(practiceScore / 20);
    const reminderOfLastBlock = practiceScore % 20;
    const percentageToColorOfLastBlock = reminderOfLastBlock * 5;
    const colorOfProgress = props.scoreColor;

    const colorChooser = colorOfProgress === 'SUCCESS_PRIMARY' ? '#2d723c' : '#d8d32f';

    const style = {
        backgroundImage: `-webkit-linear-gradient(
            left,
            ${colorChooser},
            ${colorChooser} ${percentageToColorOfLastBlock}%,
            transparent ${percentageToColorOfLastBlock}%,
            transparent 100%
        )`
    };
    const styleFullBlock = { backgroundColor: `${colorChooser}` };

    const blocks = [];

    for (let i = 0; i < 5; i++) {
        let block;
        if (i < blocksToColor) {
            block = (<div key={i} style={styleFullBlock} />)
        } else if (i === blocksToColor) {
            block = (<div key={i} style={style} />)
        } else {
            block = (<div key={i} />)
        }

        blocks.push(
            block
        );
    }

    return (
        <div className="progressbar-container" >
            {blocks}
        </div>
    );

}

export default ProgressWithSection
