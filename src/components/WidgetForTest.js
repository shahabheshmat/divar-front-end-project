import widgetData from '../Json/Widget.json'

const widget = () => {
    const { data: {icon: { image_url_dark: imageUrlDark } }} = widgetData;
    const {data : {title}} = widgetData;
    const imageComponent = (<img src={imageUrlDark} alt="success check mark" />);
    return (
        <div className="widget">
            <p dangerouslySetInnerHTML={{__html: title}}></p> 
            {imageComponent}
        </div>
    )
}

export default widget
