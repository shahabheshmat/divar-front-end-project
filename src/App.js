import './App.css';
import ScoreRow from './components/ScoreRow';
import SectionDividerRow from './components/SectionDividerRow';
// this widget is practice number one for divar front end. you can uncomment it in the return statement to see it.
// import WidgetFirstPractice from "./components/WidgetForTest";
import TitleRow from "./components/TitleRow";
import UnexpandableRow from './components/UnexpandableRow';
import listOfWidgets from "./Json/ListOfWidgets.json";


function App() {

    const children = listOfWidgets.map((widget, index) => {
        switch (widget.widget_type) {
            case 'TITLE_ROW':
                return (
                    <TitleRow key={index} text={widget.data.text} />
                );

            case 'UNEXPANDABLE_ROW':
                return (
                    <UnexpandableRow key={index} title={widget.data.title} value={widget.data.value} />
                );

            case 'SECTION_DIVIDER_ROW':
                return (
                    <SectionDividerRow key={index} />
                );

            case 'SCORE_ROW':
                return (
                    <ScoreRow
                        key={index}
                        imageUrlDark={widget.data.icon.image_url_dark}
                        percentageScore={widget.data.percentage_score}
                        scoreColor={widget.data.score_color}
                        title={widget.data.title}
                        hasDivider={widget.data.has_divider}
                    />
                );
            default:
                console.log("something went wrong :-) please be patient while we address this issue");
                return null;
        }

    });


    return (
        <div className="header">
            {/* <WidgetFirstPractice/> */}
            {/* {child} */}
            {children}
        </div>
    );
}

export default App;
